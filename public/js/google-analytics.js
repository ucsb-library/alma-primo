/**
 * Begin Google Analytics
 * Source: https://github.com/NYULibraries/primo-explore-google-analytics
 */
app.constant('googleAnalyticsConfig', {
  trackingId: 'G-R43H00TG6K',
  // use null to specify an external script shouldn't be loaded
  externalScriptURL: 'https://www.googletagmanager.com/gtag/js?id=G-R43H00TG6K',
  inlineScript: `
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'G-R43H00TG6K');
  `,
  target: 'head',
})

app.run(runBlock);
runBlock.$inject = ['gaInjectionService'];
function runBlock(gaInjectionService) {
  gaInjectionService.injectGACode();
}
/**
 * End Google Analytics
 */
